# Anna: Slackbot for GPT-2

Run go-gpt2d to listen on localhost:10101 for prompts for GPT-2

Run go-voice to listen on localhost:10102 for text to speak with Tacotron2

Run go-face to generate random faces on the console using StyleGAN

Run go-slacker to listen to Slack and send prompts to gpt2d, returning the
reply to Slack and sending it to voice (if available).

Type 'help' in a Slack DM to see some options.

 * Insane and offensive dreaming brain by [GPT-2](https://github.com/openai/gpt-2)
 * Realistic text-to-speech by [Tacotron2](https://pytorch.org/hub/nvidia_deeplearningexamples_tacotron2/)
 * Faces by [StyleGAN](https://github.com/NVlabs/stylegan)
 * Sentiment analysis performed with [nltk](https://www.nltk.org/)
