#!/usr/bin/env python3
""" Listen on localhost:10101 and chat with gpt-2 """
import warnings
warnings.filterwarnings("ignore")

import json
import os
import socket
import sys
import time
import string

import fire

import numpy as np
import tensorflow as tf

import model, sample, encoder

from nltk.tokenize import sent_tokenize

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('localhost', 10101)
print('starting up on {} port {}'.format(*server_address))
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

MODELS = '/home/rob/gpt-2/models'

trash = (
    'Trump', 'Clinton', 'Obama', 'Cruz', 'gamergate', 'election', 'Russia',
    'Jew', 'Nazi', 'racist', 'ISIS', 'caliphate', 'FoxNews', 'Cosby',
    'Confederate', 'Civil War', 'Muslim', 'Christian',
    'Advertisement', 'RAW Paste Data', 'Download', 'License',
)

def interact_model(
#    model_name='117M',
#    model_name='345M',
#    model_name='774M',
    model_name='1558M',
    seed=None,
    nsamples=3,
    batch_size=3,
    length=None,
    temperature=0.96,
    top_k=80,
):
    if batch_size is None:
        batch_size = 1
    assert nsamples % batch_size == 0
    np.random.seed(seed)
    tf.compat.v1.set_random_seed(seed)

    enc = encoder.get_encoder(model_name, MODELS)
    hparams = model.default_hparams()
    with open(os.path.join(MODELS, model_name, 'hparams.json')) as f:
        hparams.override_from_dict(json.load(f))

    if length is None:
        length = hparams.n_ctx // 2
    elif length > hparams.n_ctx:
        raise ValueError("Can't get samples longer than window size: %s" % hparams.n_ctx)

    with tf.compat.v1.Session(graph=tf.Graph()) as sess:
        print("\n>>>> Got session.")
        context = tf.compat.v1.placeholder(tf.int32, [batch_size, None])
        print("\n>>>> Got context.")
        output = sample.sample_sequence(
            hparams=hparams, length=length,
            context=context,
            batch_size=batch_size,
            temperature=temperature, top_k=top_k
        )
        print("\n>>>> Got sequence.")

        saver = tf.compat.v1.train.Saver()
        print("\n>>>> Got saver.")
        ckpt = tf.train.latest_checkpoint(os.path.join(MODELS, model_name))
        print("\n>>>> Got checkpoint.")
        saver.restore(sess, ckpt)
        print("\n>>>> Restored.")

        while True:
            try:
                print("\n>>>> Listening for connections.")
                connection, client_address = sock.accept()
                chunks = []
                count = 0
                while True:
                    msg = connection.recv(16)
                    try:
                        data = msg.decode('utf-8')
                    except UnicodeDecodeError:
                        data = msg.decode('ISO-8859-1')

                    # Junk from Slack?
                    data = data.replace('â', '-')

                    if '\n' in data:
                        # data = data[:data.find('\n')]
                        chunks.append(data.replace('\n', ' '))
                        break
                    else:
                        chunks.append(data)
                    count = count + len(data)

                    if count > 4096:
                        break

                    if not data:
                        break

                question = ''.join(chunks)

                # Printable only
                question = ''.join(filter(lambda x: x in string.printable, question))

                print('<<<', question)

                if not question:
                    continue

                # gpt2d.py can hang the CPU(!) on the current nvidia driver.
                # Better not exit.

                # if question == ':skull: :boom:':
                #     raise SystemExit('skull go boom')

                start_time = time.time()

                context_tokens = enc.encode(question)

                responses = {}
                while not responses:
                    for _ in range(nsamples // batch_size):
                        out = sess.run(output, feed_dict={
                            context: [context_tokens for _ in range(batch_size)]
                        })[:, len(context_tokens):]
                        for i in range(batch_size):
                            print(f'>>>> {i} : {round(time.time() - start_time, 2)} <<<<')
                            text = enc.decode(out[i])

                            # Printable only
                            text = ''.join(filter(lambda x: x in string.printable, text))

                            # Thought crime!
                            if any(t.lower() in text.lower() for t in trash):
                                print('Trash detected, skipping...')
                                continue

                            # Normalization, people. Yeesh.
                            if '<|endoftext|>' in text:
                                text = text[0:text.find('<|endoftext|>')]

                            # More encoding junk
                            text = text.replace('—', '-')
                            text = text.replace('~', '')

                            # Last sentence is always a fragment, so skip it
                            text = text[:text.rfind('\n')]

                            # Don't repeat yourself
                            sentences = []
                            for sentence in sent_tokenize(text):
                                if sentence in sentences:
                                    continue
                                else:
                                    sentences.append(sentence)

                            text = ' '.join(sentences)

                            if text:
                                score = sum([text.count(c) for c in [':', '?', '*', '_', '\n']]) * 10000 // len(text)
                                responses[score] = text.encode('utf-8')
                                print('>>>> considering', score, ':', text)

                winner = min(responses.keys())
                print('\n>>>> winner is', winner, ':', responses[winner])

                connection.sendall(responses[winner])
                connection.close()

            except (KeyboardInterrupt, EOFError):
                print('\n')
                break

if __name__ == '__main__':
    fire.Fire(interact_model)

