#!/usr/bin/env python3

import os
import re
import logging
import string
import slack
import sys
import ssl as ssl_lib
import certifi
import asyncio
import socket
import random
import multiprocessing

from time import sleep
from random import randint, choice

from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.sentiment.vader import SentimentIntensityAnalyzer

sid = SentimentIntensityAnalyzer()
min_score = -0.3

emoji_on = False

ToT = ""
convo = {}

GPT_IN = multiprocessing.JoinableQueue()
GPT_OUT = multiprocessing.JoinableQueue()

spectrum = [
    ':imp:', ':angry:', ':rage:', ':triumph:', ':scream:', ':tired_face:',
    ':sweat:', ':cold_sweat:', ':fearful:', ':sob:', ':weary:', ':cry:', ':mask:',
    ':confounded:', ':persevere:', ':unamused:', ':confused:', ':dizzy_face:',
    ':disappointed_relieved:', ':disappointed:', ':worried:', ':anguished:',
    ':frowning:', ':astonished:', ':flushed:', ':open_mouth:', ':hushed:',
    ':pensive:', ':expressionless:', ':neutral_face:', ':grimacing:',
    ':no_mouth:', ':kissing:', ':relieved:', ':smirk:', ':relaxed:',
    ':simple_smile:', ':blush:', ':wink:', ':sunglasses:', ':yum:',
    ':stuck_out_tongue:', ':stuck_out_tongue_closed_eyes:',
    ':stuck_out_tongue_winking_eye:', ':smiley:', ':smile:', ':laughing:',
    ':sweat_smile:', ':joy:', ':grin:'
]

def get_emoji(sentence):
    score = sid.polarity_scores(sentence)['compound']
    return spectrum[int(((score + 1) / 2) * (len(spectrum) - 1))]

def random_emoji():
    return choice((
        ':bowtie:', ':smile:', ':simple_smile:', ':laughing:', ':blush:', ':smiley:', ':relaxed:',
        ':smirk:', ':heart_eyes:', ':kissing_heart:', ':kissing_closed_eyes:', ':flushed:',
        ':relieved:', ':satisfied:', ':grin:', ':wink:', ':stuck_out_tongue_winking_eye:',
        ':stuck_out_tongue_closed_eyes:', ':grinning:', ':kissing:', ':kissing_smiling_eyes:',
        ':stuck_out_tongue:', ':sleeping:', ':worried:', ':frowning:', ':anguished:',
        ':open_mouth:', ':grimacing:', ':confused:', ':hushed:', ':expressionless:', ':unamused:',
        ':sweat_smile:', ':sweat:', ':disappointed_relieved:', ':weary:', ':pensive:',
        ':disappointed:', ':confounded:', ':fearful:', ':cold_sweat:', ':persevere:', ':cry:',
        ':sob:', ':joy:', ':astonished:', ':scream:', ':tired_face:', ':angry:',
        ':rage:', ':triumph:', ':sleepy:', ':yum:', ':mask:', ':sunglasses:', ':dizzy_face:',
        ':imp:', ':smiling_imp:', ':neutral_face:', ':no_mouth:', ':innocent:', ':alien:',
        ':yellow_heart:', ':blue_heart:', ':purple_heart:', ':heart:', ':green_heart:',
        ':broken_heart:', ':heartbeat:', ':heartpulse:', ':two_hearts:', ':revolving_hearts:',
        ':cupid:', ':sparkling_heart:', ':sparkles:', ':star:', ':star2:', ':dizzy:', ':boom:',
        ':collision:', ':anger:', ':exclamation:', ':question:', ':grey_exclamation:',
        ':grey_question:', ':zzz:', ':dash:', ':sweat_drops:', ':notes:', ':musical_note:',
        ':fire:', ':shit:', ':+1:', ':-1:',
        ':ok_hand:', ':punch:', ':fist:', ':v:', ':wave:', ':hand:',
        ':raised_hand:', ':open_hands:', ':point_up:', ':point_down:', ':point_left:',
        ':point_right:', ':raised_hands:', ':pray:', ':point_up_2:', ':clap:', ':muscle:',
        ':the_horns:', ':middle_finger:'
    ))

class gpt(multiprocessing.Process):
    def __init__(self, inq, outq):
        super().__init__()
        self.inq = inq
        self.outq = outq

    def run(self):
        while True:
            message = self.inq.get()
            if message is None:
                break
            else:
                try:
                    sock = socket.create_connection(('localhost', 10101))
                    sock.settimeout(100)
                    sock.sendall(message.encode('utf-8'))
                    sock.sendall(b'\n')

                    print(f"<<< '{message}' sent to GPT...")

                    chunks = []
                    data = sock.recv(16)
                    while data:
                        try:
                            chunks.append(data.decode('utf-8'))
                        except UnicodeDecodeError:
                            chunks.append(data.decode('ISO-8859-1'))
                        data = sock.recv(16)
                    sock.close()
                    self.outq.put(''.join(chunks).split('\n'))

                except socket.timeout:
                    self.outq.put([random_emoji()])

@slack.RTMClient.run_on(event="reaction_added")
def update_emoji(**payload):
    """Update onboarding welcome message after receiving a "reaction_added"
    event from Slack.
    """
    global ToT
    data = payload["data"]
    web_client = payload["web_client"]
    channel_id = data["item"]["channel"]
    user_id = data["user"]

    try:
        # get the relevant message
        response = web_client.conversations_history(
            channel = channel_id,
            latest = data["item"]["ts"],
            limit = 1,
            inclusive = "true"
        )
    except slack.errors.SlackApiError:
        return

    if response["ok"]:
        ToT = response["messages"][0]["text"]

    print(f">>> ToT = {ToT}")

 # arbitrary api command:
 # |      client = slack.WebClient(token=os.environ['SLACK_API_TOKEN'])
 # |      response = client.api_call(
 # |          api_method='chat.postMessage',
 # |          json={'channel': '#random','text': "Hello world!"}
 # |      )
 # |      assert response["ok"]
 # |      assert response["message"]["text"] == "Hello world!"

def speak(message):
    try:
        sock = socket.create_connection(('localhost', 10102))
        sock.settimeout(5)
        sock.sendall(message.encode('utf-8'))
        sock.sendall(b'\n')
        sock.close()

        print(f"<<< sent to tts...")

    except Exception:
        print(f">>> connect to tts failed.")


@slack.RTMClient.run_on(event="message")
async def message(**payload):
    global ToT, min_score, emoji_on, convo
    data = payload["data"]
    web_client = payload["web_client"]
    rtm_client = payload["rtm_client"]
    channel_id = data.get("channel")
    user_id = data.get("user")
    text = data.get("text")

    junk = ('Advertisement', 'RAW Paste Data', 'Download', 'License', 'Q', 'Q.', 'Q:', 'A', 'A.', 'A:', '')

    async def say(channel_id, message, typing=True):
        if not message:
            return

        if typing:
            await rtm_client.typing(channel=channel_id)

        # post the message to Slack
        return await web_client.chat_postMessage(
            channel = channel_id,
            text = message
        )

    if channel_id not in convo:
        convo[channel_id] = {}

    # Don't talk to bots (or myself)
    if 'user' in data:
        print(f"{user_id}: {text}")

        if user_id not in convo[channel_id]:
            convo[channel_id][user_id] = {}
            convo[channel_id][user_id]['context'] = None
            convo[channel_id][user_id]['overflow'] = []

        # while len(convo[channel_id][user_id]['context']) > 2:
        #     convo[channel_id][user_id]['context'].pop(0)

        if text == 'help':
            await say(channel_id, f'''Cheat codes:
'forget it' or 'never mind' : clear short term memory
min_score xx : set maximum negativity (-1.0 to 1.0)
:thought_balloon: : show what's on my mind
:dark_sunglasses: : enable / disable sentiment analysis emoji
:see_no_evil: : enable / disable face generation
:hear_no_evil: : enable / disable speech recognition (TBD)
:speak_no_evil: : enable / disable voice generation
:skull: :bomb: : reboot slacker
''')
            return

        if text == ':skull: :bomb:':
            await say(channel_id, f':skull_and_crossbones:', False)
            GPT_IN.put(None)
            sys.exit(0)

        if text == ':see_no_evil:':
            if os.path.exists('.faces-enabled'):
                os.remove('.faces-enabled')
                await say(channel_id, f":monkey_face: :-1:")
            else:
                with open('.faces-enabled', 'w') as f:
                    await say(channel_id, f":monkey_face: :+1:")
            return

        if text == ':hear_no_evil:':
            if os.path.exists('.audio-enabled'):
                os.remove('.audio-enabled')
                await say(channel_id, f":monkey_face: :-1:")
            else:
                with open('.audio-enabled', 'w') as f:
                    await say(channel_id, f":monkey_face: :+1:")
            return

        if text == ':speak_no_evil:':
            if os.path.exists('.voice-enabled'):
                os.remove('.voice-enabled')
                await say(channel_id, f":monkey_face: :-1:")
            else:
                with open('.voice-enabled', 'w') as f:
                    await say(channel_id, f":monkey_face: :+1:")
            return

        if 'has joined the channel' in text:
            await say(channel_id, f"{random_emoji()} <@{user_id}>")
            print(f"{random_emoji()} {user_id}")
            return

        if 'set the channel topic:' in text:
            return

        # clear convo
        if text.lower() in ('forget it', 'never mind'):
            await say(channel_id, f"OK, <@{user_id}>. All is forgotten.", False)
            convo[channel_id][user_id]['context'] = None
            convo[channel_id][user_id]['overflow'] = []
            ToT = ""
            print(f"forgot {user_id}")
            return

        # sentiment analysis
        if text == ':dark_sunglasses:':
            if emoji_on:
                emoji_on = False
                await say(channel_id, f"OK, <@{user_id}>. I'll hide my feels. :face_with_hand_over_mouth:", False)
            else:
                emoji_on = True
                await say(channel_id, f"OK, <@{user_id}>. :two_hearts:", False)

            print(f"emoji_on: {emoji_on}")
            return

        # this convo
        if text  == ':thought_balloon:':
            for u in convo[channel_id]:
                await say(channel_id, f"<@{u}>: {convo[channel_id][u]}", False)
                await say(channel_id, f"ToT: {ToT}, min_score {min_score}, emoji_on {emoji_on}", False)
                await say(channel_id, f"faces: {os.path.exists('.faces-enabled')}, voice: {os.path.exists('.voice-enabled')}", False)
            print(convo[channel_id])
            return

        # all convos
        if text == ':mag_right:':
            for c in convo:
                for u in convo[c]:
                    await say(channel_id, f"<@{c}>: <@{u}>: {convo[c][u]}", False)
            await say(channel_id, f"ToT: {ToT}, min_score {min_score}", False)
            return

        # adjust the min_score
        if text == 'min_score':
            await say(channel_id, f"min_score == {min_score}", False)
            return

        match = re.search(r'^min_score (.*)', text)
        if match:
            try:
                min_score = float(match.group(1))
                min_score = max(-1.0, min_score)
                min_score = min(1.0, min_score)
                print(f'>>> min_score == {min_score}')
                await say(channel_id, f"OK, <@{user_id}>. min_score == {min_score}", False)
            except Exception:
                print(f'>>> invalid min_score {match.group(1)}')
                await say(channel_id, f'You want min_score == {match.group(1)}? :thinking_face:')
            return

        # just an emoji
        if re.findall('^:\S+:$', text):
            await say(channel_id, f"{random_emoji()}")
            print(f"{random_emoji()}")
            return

        await rtm_client.typing(channel=channel_id)

        convo[channel_id][user_id]['context'] = text.replace('\n', ' ')

        GPT_IN.put(f"{ToT} {convo[channel_id][user_id]['context']}".lstrip())

        # gpt takes ~30 seconds. so continue the thought to tide them over.
        await rtm_client.typing(channel=channel_id)

        if convo[channel_id][user_id]['overflow']:
            maxlines = randint(2, 7)
            pause = 30.0 / min(maxlines, len(convo[channel_id][user_id]['overflow']))
            for i, line in enumerate(convo[channel_id][user_id]['overflow']):
                await rtm_client.typing(channel=channel_id)
                sleep(pause + randint(0, 2))
                # print(f"---{line}---")
                await say(channel_id, line)
                if i > maxlines:
                    convo[channel_id][user_id]['overflow'] = []
                    break

        paragraphs = GPT_OUT.get()

        print(convo)

        # keep it short-ish
        maxlines = randint(1, 4)
        maxchars = 150

        response = []
        overflow = []

        while(paragraphs):
            paragraph = paragraphs.pop(0)
            rejoinder = []

            # shouldn't be necessary since gptd does filtering, but just in case
            paragraph = ''.join(filter(lambda x: x in string.printable, paragraph))

            # skip junk
            if paragraph in junk:
                continue

            # break paragraph into sentences
            sentences = sent_tokenize(paragraph)
            while sentences:
                sentence = sentences.pop(0)

                if sentence in junk:
                    continue

                # (1)
                # 2)
                # 3.
                # 4
                if re.findall(r'^\(?\d+\.?\)?$', text):
                    continue

                # sentiment cutoff
                score = sid.polarity_scores(sentence)['compound']
                if score < min_score:
                    print(f'>>> {score} < {min_score}, skipping "{sentence}"')
                    continue

                if sentence:
                    if maxlines < 0 or maxchars < 0:
                        sentences.insert(0, sentence)
                        overflow.extend(sentences)
                        overflow.extend(paragraphs)
                        break

                    maxlines = maxlines - 1
                    maxchars = maxchars - len(sentence)

                    rejoinder.append(sentence)
                    ToT = sentence

                    # sentiment
                    if emoji_on:
                        rejoinder.append(get_emoji(sentence))


            if rejoinder:
                response.append(rejoinder)

        # random emoji is funny
        if not response:
            response.append([random_emoji()])

        convo[channel_id][user_id]['overflow'] = overflow

        for r in response:
            print(f">>> responding: {' '.join(r)}")
            await rtm_client.typing(channel=channel_id)
            sleep(randint(1,2))

            if emoji_on:
                words = ' '.join(r[:-1])
            else:
                words = ' '.join(r)

            speak(words)
            await say(channel_id, words)

        if ToT:
            print(f'>>> ToT: {ToT}')

        if overflow:
            print(f'>>> overflow: {overflow}')
       # else:
       #     print(f'>>> out of overflow, pre-caching "{ToT}"')
       #     convo[channel_id][user_id]['overflow'] = get_gpt(ToT)
       #     await say(channel_id, '…')

        print('\n>>> waiting for conversation')

if __name__ == "__main__":
    ssl_context = ssl_lib.create_default_context(cafile=certifi.where())
    slack_token = os.environ["SLACK_BOT_TOKEN"]

    print('>>> connected to Slack')
    print('>>> waiting for conversation')

    # See the tutorial:
    # https://github.com/slackapi/python-slackclient/blob/master/tutorial/PythOnBoardingBot/async_app.py

    gptworker = gpt(GPT_IN, GPT_OUT)
    gptworker.start()

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    try:
        rtm_client = slack.RTMClient(token=slack_token, ssl=ssl_context, run_async=True, loop=loop)
        loop.run_until_complete(rtm_client.start())
    except Exception:
        GPT_IN.put(None)
        gptworker.terminate()
