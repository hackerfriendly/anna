#!/usr/bin/env python3

import re
import string

import warnings
warnings.filterwarnings("ignore")

import fileinput
from subprocess import run

import sys
sys.path.append('waveglow/')

from io import BytesIO

import numpy as np

import torch

from nltk.tokenize import sent_tokenize, word_tokenize

from hparams import create_hparams
from model import Tacotron2
from layers import TacotronSTFT, STFT
from audio_processing import griffin_lim
from train import load_model
from text import text_to_sequence
from denoiser import Denoiser
from time import sleep

from gtts import gTTS

import os
import random
import signal
import socket
import select

port = 10102

def handler(signum, frame):
    raise Exception("Thinking WAY too hard about that.")

signal.signal(signal.SIGALRM, handler)

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('localhost', port)
print('starting up on {} port {}'.format(*server_address))
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(server_address)

# Listen for incoming connections
sock.listen(5)

hparams = create_hparams()
hparams.sampling_rate = 22050
hparams.max_decoder_steps=10000

checkpoint_path = "tacotron2_statedict.pt"
model = load_model(hparams)
model.load_state_dict(torch.load(checkpoint_path)['state_dict'])
_ = model.cuda().eval().half()

waveglow_path = 'waveglow_256channels.pt'
waveglow = torch.load(waveglow_path)['model']
waveglow.cuda().eval().half()
for k in waveglow.convinv:
    k.float()
denoiser = Denoiser(waveglow)

read_list = [sock]
ANNA = False

ACCENTS = ["com.au", "co.uk", "com", "ca", "co.in", "ie", "co.za"]
ACCENT = "com"

while True:
    text = None
    try:
        print(f">>> tts.py: Listening for connections")
        readable, writable, errored = select.select(read_list, [], [])
        for s in readable:
            if s is sock:
                client_sock, client_address = sock.accept()
                read_list.append(client_sock)
                print(f'connection from: {client_address}')
            else:
                chunks = []
                count = 0
                while True:
                    msg = s.recv(16)
                    try:
                        data = msg.decode('utf-8')
                    except UnicodeDecodeError:
                        data = msg.decode('ISO-8859-1')

                    chunks.append(data)

                    count = count + len(data)
                    if count > 4096:
                        break

                    if not data:
                        break

                s.close()
                read_list.remove(s)

                text = ''.join(chunks).replace('\n', ' ')
                
                if text.startswith('ANNA:'):
                    ANNA = True
                    text = text[5:]
                else:
                    ANNA = False
                    ACCENT = random.choice(ACCENTS)
                    print("Language:", ACCENT)
#                print('>>> ', text)

        if not text:
            continue

        for sentence in sent_tokenize(text):
            # If you can't sound it out it in 30 seconds, time to bail.
            signal.alarm(30)
            
            sentence = re.sub(f'[^{string.ascii_letters}{string.digits} ,:-_.?!\']', '', sentence)
            if not sentence or sentence == "...":
                continue

            sequence = np.array(text_to_sequence(sentence, ['english_cleaners']))[None, :]
            sequence = torch.autograd.Variable(torch.from_numpy(sequence)).cuda().long()

            mel_outputs, mel_outputs_postnet, _, alignments = model.inference(sequence)

            with torch.no_grad():
                audio = waveglow.infer(mel_outputs_postnet, sigma=0.666)

            audio_denoised = denoiser(audio, strength=0.01)[:, 0]
            #print(sentence)

            signal.alarm(0)

            #run(f'figlet -f slant -w 120 | lolcat', input=sentence.encode('utf-8'), shell=True)

            # Always icecast
            if ANNA:
                print("Anna:", sentence)
                run(
                    ["aplay", "-r", str(hparams.sampling_rate), "-f", "FLOAT_LE"],
                    input=audio_denoised.squeeze().cpu().numpy().tobytes(),
                    shell=False
                )
            else:
                print(f"Them ({ACCENT}):", sentence)
                tts = gTTS(sentence, lang='en', tld=ACCENT)
                mp3 = BytesIO()
                tts.write_to_fp(mp3)
                run(
                    ["mpg123", "-"],
                    input=mp3.getvalue(),
                    shell=False
                )
                mp3.close()

                # run(f'gtts-cli -l {ACCENT} "{sentence}" | mpg123 - > /dev/null 2>&1', shell=True)


            # Optionally speak out loud
            if os.path.exists('../../.voice-enabled'):
                if ANNA:
                    run(
                        ["aplay", "-r", str(hparams.sampling_rate), "-f", "FLOAT_LE", "-D", "default:CARD=Audio"],
                        input=audio_denoised.squeeze().cpu().numpy().tobytes(),
                        shell=False
                    )
                else:
                    tts = gTTS(sentence, lang='en', tld=ACCENT)
                    mp3 = BytesIO()
                    tts.write_to_fp(mp3)
                    run(
                        ["mpg123", "-a", "default:CARD=Audio", "-"],
                        input=mp3.getvalue(),
                        shell=False
                    )
                    mp3.close()

                    # run(f'gtts-cli -l {ACCENT} "{sentence}" | mpg123 -a "default:CARD=Audio" - > /dev/null 2>&1', shell=True)
            else:
                print('(shhhh!)')

    except (KeyboardInterrupt, EOFError):
        print('\n')
        break
