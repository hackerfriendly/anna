#!/bin/bash
if [ -z "$SLACK_BOT_TOKEN" ]; then
    echo "Please set SLACK_BOT_TOKEN first."
    exit 1
fi

if [ -z "$3" ]; then
    echo "Usage: `basename $0` channel text image_url"
    exit 1
fi

TEXT=${2/\"/}
BLOCKS="[{\"type\":\"image\",\"title\":{\"type\":\"plain_text\",\"text\":\"${TEXT}\"},\"image_url\":\"$3\",\"alt_text\":\"${TEXT}\"}]"

curl --request POST \
     --data-urlencode "token=$SLACK_BOT_TOKEN" \
     --data-urlencode "channel=$1" \
     --data-urlencode "username=Anna" \
     --data-urlencode "text=${TEXT}" \
     --data-urlencode "blocks=$BLOCKS" \
     'https://slack.com/api/chat.postMessage' \
     -w "\n"
