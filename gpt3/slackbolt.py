#!/usr/bin/env python3
"""
slackbolt.py

A Slack bot based on GPT-3.
"""
import collections
import os
import random
import re
import socket
import threading as th

from datetime import (datetime, timezone)
from subprocess import run

import urllib3

import openai

from slack_bolt import App # pylint: disable=import-error
from slack_bolt.adapter.socket_mode import SocketModeHandler # pylint: disable=import-error

# for offline sentiment analysis
from nltk.sentiment.vader import SentimentIntensityAnalyzer

# long term memory
from elasticsearch import Elasticsearch

# Disable SSL warnings for Elastic
urllib3.disable_warnings()

app = App(token=os.environ["SLACK_BOT_TOKEN"])

openai.api_key = os.getenv("OPENAI_API_KEY")

ELASTIC_KEY = os.environ.get('ELASTIC_KEY', None)
ELASTIC_INDEX = "anna-v0"

# My name. TODO: get this from the Slack API.
ME = "Anna"

IMAGE_ENGINES = ["/home/rob/anna/v-diffusion-pytorch/go-vdiff", "/home/rob/anna/clip/go-clip"]
IMAGE_ENGINE_WEIGHTS = [0.666, 0.333]

# The default model. At the moment, pricing is as follows (per 1k tokens in the prompt or response).
# Bigger models are more coherent but much more pricey:
#
# davinci: $0.06
#   curie: $0.006
# babbage: $0.0012
#     ada: $0.0008
DEFAULT_MODEL = "davinci"
# DEFAULT_MODEL = "curie"

# Sentiment analysis
sid = SentimentIntensityAnalyzer()

# Long-term memory
es = Elasticsearch(["https://aramaki.s9:9200"], http_auth=(ME, ELASTIC_KEY), verify_certs=False)

# How are we feeling today?
feels = {'current': 0.15, 'default': 0.15} # expressionless

# Length of the Short Term Memory. Bigger == more coherent == $$$
STM = 16

known_users = {}
ToT = { 'memories': {} }

def random_emoji():
    ''' :wink: '''
    return random.choice((
        ':bowtie:', ':smile:', ':simple_smile:', ':laughing:', ':blush:', ':smiley:', ':relaxed:',
        ':smirk:', ':heart_eyes:', ':kissing_heart:', ':kissing_closed_eyes:', ':flushed:',
        ':relieved:', ':satisfied:', ':grin:', ':wink:', ':stuck_out_tongue_winking_eye:',
        ':stuck_out_tongue_closed_eyes:', ':grinning:', ':kissing:', ':kissing_smiling_eyes:',
        ':stuck_out_tongue:', ':sleeping:', ':worried:', ':frowning:', ':anguished:',
        ':open_mouth:', ':grimacing:', ':confused:', ':hushed:', ':expressionless:', ':unamused:',
        ':sweat_smile:', ':sweat:', ':disappointed_relieved:', ':weary:', ':pensive:',
        ':disappointed:', ':confounded:', ':fearful:', ':cold_sweat:', ':persevere:', ':cry:',
        ':sob:', ':joy:', ':astonished:', ':scream:', ':tired_face:', ':angry:',
        ':rage:', ':triumph:', ':sleepy:', ':yum:', ':mask:', ':sunglasses:', ':dizzy_face:',
        ':imp:', ':smiling_imp:', ':neutral_face:', ':no_mouth:', ':innocent:', ':alien:',
        ':yellow_heart:', ':blue_heart:', ':purple_heart:', ':heart:', ':green_heart:',
        ':broken_heart:', ':heartbeat:', ':heartpulse:', ':two_hearts:', ':revolving_hearts:',
        ':cupid:', ':sparkling_heart:', ':sparkles:', ':star:', ':star2:', ':dizzy:', ':boom:',
        ':collision:', ':anger:', ':exclamation:', ':question:', ':grey_exclamation:',
        ':grey_question:', ':zzz:', ':dash:', ':sweat_drops:', ':notes:', ':musical_note:',
        ':fire:', ':shit:', ':+1:', ':-1:',
        ':ok_hand:', ':punch:', ':fist:', ':v:', ':wave:', ':hand:',
        ':raised_hand:', ':open_hands:', ':point_up:', ':point_down:', ':point_left:',
        ':point_right:', ':raised_hands:', ':pray:', ':point_up_2:', ':clap:', ':muscle:',
        ':the_horns:', ':middle_finger:'
    ))

# Behold the emoji emotional spectrum
spectrum = [
    ':imp:', ':angry:', ':rage:', ':triumph:', ':scream:', ':tired_face:',
    ':sweat:', ':cold_sweat:', ':fearful:', ':sob:', ':weary:', ':cry:', ':mask:',
    ':confounded:', ':persevere:', ':unamused:', ':confused:', ':dizzy_face:',
    ':disappointed_relieved:', ':disappointed:', ':worried:', ':anguished:',
    ':frowning:', ':astonished:', ':flushed:', ':open_mouth:', ':hushed:',
    ':pensive:', ':expressionless:', ':neutral_face:', ':grimacing:',
    ':no_mouth:', ':kissing:', ':relieved:', ':smirk:', ':relaxed:',
    ':simple_smile:', ':blush:', ':wink:', ':sunglasses:', ':yum:',
    ':stuck_out_tongue:', ':stuck_out_tongue_closed_eyes:',
    ':stuck_out_tongue_winking_eye:', ':smiley:', ':smile:', ':laughing:',
    ':sweat_smile:', ':joy:', ':grin:'
]

def cast(message):
    ''' Cast to icecast, maybe speak out loud '''
    try:
        sock = socket.create_connection(('localhost', 10102))
        sock.settimeout(5)
        sock.sendall(message.encode('utf-8'))
        sock.sendall(b'\n')
        sock.close()
        print(f"<<< sent to tts: {message}")
    except Exception:
        print(">>> connect to tts failed.")

def get_spectrum(score):
    ''' Translate a score from -1 to 1 into an emoji on the spectrum '''
    return spectrum[int(((score + 1) / 2) * (len(spectrum) - 1))]

def new_channel(channel):
    ''' Initialize a new channel. '''
    ToT[channel] = {
        'rejoinder': th.Timer(0, print, ['New channel:', channel]),
        'convo': collections.deque(maxlen=STM)
    }

    # Try to load channel history from Elasticsearch
    history = es.search( # pylint: disable=unexpected-keyword-arg
        index=ELASTIC_INDEX,
        query={
            "term": {"channel.keyword": channel}
        },
        sort=[{"@timestamp":{"order":"desc"}}],
        size=STM
    )['hits']['hits']

    if history:
        for line in history[::-1]:
            ToT[channel]['convo'].append(line['_source']['msg'])

    ToT[channel]['rejoinder'].start()

def get_display_name(user_id):
    """ Return the user's first name if available, otherwise the display name """
    if user_id not in known_users:
        info = app.client.users_info(user=user_id)['user']
        try:
            known_users[user_id] = info['profile']['first_name']
        except KeyError:
            known_users[user_id] = info['profile']['display_name']

    return known_users[user_id]

def substitute_names(text):
    """ Substitute all <@XYZ> in text with the equivalent display name. """
    for user_id in set(re.findall(r'<@(\w+)>', text)):
        text = re.sub(f'<@{user_id}>', get_display_name(user_id), text)
    return text

def get_channel_members(channel):
    """ Return the list of member names for people actually speaking in a given channel """

    # Ideally this would include everyone, but the stop list can only contain 4 entries.
    # Just return three random speakers and ME instead.
    return [ME] + list({x.split(':')[0] for x in ToT[channel]['convo']} - {ME})[:3]

def take_a_photo(channel, text):
    ''' Pick an image engine and generate a photo '''
    engine = random.choices(
        IMAGE_ENGINES,
        weights=IMAGE_ENGINE_WEIGHTS
    )

    run(
        [engine[0], channel, text],
        check=False
    )

def save_to_ltm(channel, them, msg):
    ''' Save convo to ElasticSearch '''
    doc = {
        "@timestamp": str(datetime.now(timezone.utc).astimezone().isoformat()),
        "channel": channel,
        "speaker": them,
        "msg": msg
    }
    _id = es.index(index=ELASTIC_INDEX,  document=doc)["_id"] # pylint: disable=unexpected-keyword-arg, no-value-for-parameter
    print("doc:", _id)

    return _id

def get_gpt_response(prompt, stop=None, temperature=0.9, max_tokens=200, engine=DEFAULT_MODEL):
    """ Send the prompt to GPT and return the response """
    response = openai.Completion.create(
        engine=engine,
        prompt=prompt,
        temperature=temperature,
        max_tokens=max_tokens,
        n=5,
        frequency_penalty=0.5,
        presence_penalty=0.6,
        stop=stop
    )
    # print('Prompt:', prompt, '\nResponse:', response)

    # Choose a response based on the most positive sentiment.
    scored = {}
    weights = [1]
    for choice in response.choices:
        text = choice['text'].strip()
        # Skip blanks. Seriously, why would OpenAI ever return an empty string?
        if not text:
            continue
        # Skip prompt bleed-through
        if 'This is a conversation between' in text:
            continue
        if f'{ME} is feeling' in text:
            continue
        if 'http' in text:
            continue
        if text.startswith("I am feeling"):
            continue
        # Too long? Ditch the last sentence fragment.
        if choice['finish_reason'] == 'length':
            try:
                text = text[:text.rindex('.') + 1]
            except ValueError:
                pass
        scored[get_feels(choice['text'].strip())] = text
    if not scored:
        return ':shrug:'

    for item in sorted(scored.items()):
        print(f"{item[0]}:", item[1])

    # If there's only one, use it
    if len(scored) == 1:
        idx = list(scored.keys())[0]
    # If we're feeling down, try harder
    elif feels['current'] < 0:
        idx = sorted(scored.keys())[-1]
    # Otherwise choose randomly, weighing mid-high choices heavier.
    # There may not be n total since some may be filtered.
    else:
        weights = [1,] * len(scored)
        weights[0] /= 10
        weights[-1] /= 3
        idx = random.choices(list(sorted(scored.keys())), weights=weights)[0]
    reply = scored[idx]
    print("scores:", sorted(scored.keys()), "weights:", weights, "choice:", idx, reply)

    return reply

def get_gpt3_feels(prompt):
    ''' How do we feel about this conversation? Ask GPT3 and return a string. '''
    reply = get_gpt_response(
        f"{prompt}\n\nThe conversation left {ME} feeling, in a single word,",
        max_tokens=15,
        engine="curie"
    ).split('\n')[0]
    return re.split(r'[,\.\n]', reply.lower().replace('"', ''))[0]

def get_feels(prompt):
    ''' How do we feel about this conversation? Ask VADER and return a float. '''
    return sid.polarity_scores(prompt)['compound']

def get_reply(channel, them, msg):
    ''' Track the Train of Thought, and send an appropriate response. '''
    if msg != '...':
        save_to_ltm(channel, them, msg)
        ToT[channel]['convo'].append(f"{them}: {msg}")

    members = ' and '.join(get_channel_members(channel))
    recent = '\n'.join(ToT[channel]['convo'])

    prompt = f"""
This is a conversation between {members}. {ME} is feeling {get_spectrum(feels['current'])}.

{recent}
{ME}:""".strip()

    reply = get_gpt_response(prompt, stop=[f"{u}:" for u in get_channel_members(channel)])
    ToT[channel]['convo'].append(f"{ME}: {reply}")
    save_to_ltm(channel, ME, reply)

    score = get_feels(f'{prompt} {reply}')
    feels['current'] = score

    print('ToT:', ToT, '\nFeeling:', feels['current'], get_spectrum(score))

    return reply

@app.message(re.compile(r"^help$", re.I))
def help_me(say, context): # pylint: disable=unused-argument
    ''' TODO: These should really be / commands. '''
    say(f"""Commands:
  `...`: Let {ME} keep talking without interrupting
  `forget it`: Clear the conversation history for this channel
  `status`: How is {ME} feeling right now?
  `summary`: Explain it all to me in a single sentence.
  :speak_no_evil: : Enable/disable speaking at Unit 16
  :camera: _prompt_ : Generate a picture of _prompt_
""")

@app.message(re.compile(r"^forget it$", re.I))
def amnesia(say, context):
    ''' Reset feelings to default and drop the ToT for the current channel '''
    channel = context['channel_id']
    them = get_display_name(context['user_id'])

    if channel in ToT:
        ToT[channel]['rejoinder'].cancel()
        ToT[channel]['convo'].clear()

    feels['current'] = feels['default']
    print('ToT:', ToT, 'Feeling:', feels['current'])
    say(f"All is forgotten, {them}.")
    say(f"Now I feel {get_spectrum(feels['current'])}.")

@app.message(re.compile(r"^status$", re.I))
def status_report(say, context):
    ''' Set the topic or say the current feels. '''
    channel = context['channel_id']

    if channel not in ToT:
        new_channel(channel)

    # Interrupt any rejoinder in progress
    ToT[channel]['rejoinder'].cancel()

    print('ToT:', ToT, 'Feeling:', feels['current'], get_spectrum(feels['current']))
    info = app.client.conversations_info(channel=channel)

    if 'topic' in info['channel']:
        app.client.conversations_setTopic(channel=channel, topic=f"{ME} is feeling {get_spectrum(feels['current'])}.")
    else:
        say(get_spectrum(feels['current']))

    # Keep the conversation going hours later
    say_something_later(
        say,
        channel,
        context,
        when=random.randint(12 * 3600, 48 * 3600)
    )

    take_a_photo(channel, get_summary('\n'.join(ToT[channel]['convo'])))

@app.message(re.compile(r"^:speak_no_evil:$"))
def toggle(say, context): # pylint: disable=unused-argument
    ''' Local text to speech '''
    if os.path.exists('.voice-enabled'):
        os.remove('.voice-enabled')
        say(":monkey_face: :-1:")
    else:
        with open('.voice-enabled', 'w', encoding='utf-8'):
            say(":monkey_face: :+1:")

@app.message(re.compile(r"^:camera:(.+)$"))
def picture(say, context): # pylint: disable=unused-argument
    ''' Take a picture, it'll last longer '''
    them = get_display_name(context['user_id'])
    channel = context['channel_id']

    say(f"OK, {them}. _Anna takes out her camera..._\nGive me a few minutes.")
    take_a_photo(channel, context['matches'][0].strip())

@app.message(re.compile(r"^:\w+:"))
def wink(say, context): # pylint: disable=unused-argument
    ''' Every single emoji gets a emoji one back. '''
    say(random_emoji())

def get_summary(text, engine=DEFAULT_MODEL):
    ''' tl;dr: Return a condensed summary from GPT in the most ridiculous way possible.'''
    response = openai.Completion.create(
        engine=engine,
        prompt=f"{text}\n\nTo sum it up in one sentence:\n",
        temperature=0,
        max_tokens=50,
        top_p=1.0,
        frequency_penalty=0.0,
        presence_penalty=0.0
    )
    reply = response.choices[0]['text'].strip().split('\n')[0]

    if ':' in reply:
        reply = reply.split(':')[1]

    print("full summary:", reply)

    # Too long? Ditch the last sentence fragment.
    if response.choices[0]['finish_reason'] == "length":
        try:
            return reply[:reply.rindex('.') + 1]
        except ValueError:
            pass

    return reply

@app.message(re.compile(r"^summary$", re.I))
def summarize(say, context):
    ''' Say a condensed summary of the ToT for this channel '''
    # print(app.client.bots_info())
    channel = context['channel_id']
    if channel not in ToT:
        say(":shrug:")
        return

    recent = '\n'.join(ToT[channel]['convo'])
    summary = get_summary(recent)
    say(summary or ":shrug:")

def say_something_later(say, channel, context, when):
    ''' Continue the train of thought later. When is in seconds. '''
    # yadda yadda yadda
    yadda = {
        'channel_id': channel,
        'user_id': context['user_id'],
        'matches': ['...']
    }
    ToT[channel]['rejoinder'].cancel()
    ToT[channel]['rejoinder'] = th.Timer(when, catch_all, [say, yadda])
    ToT[channel]['rejoinder'].start()

@app.message(re.compile(r"(.*)", re.I))
def catch_all(say, context):
    ''' Default message handler. Prompt GPT and randomly arm a Timer for later reply. '''
    channel = context['channel_id']

    if channel not in ToT:
        new_channel(channel)

    # Interrupt any rejoinder in progress
    ToT[channel]['rejoinder'].cancel()

    them = get_display_name(context['user_id'])
    msg = substitute_names(context['matches'][0]).strip()

    cast(msg)

    # 5% of the time, say nothing (for now).
    if random.random() < 0.95:
        the_reply = get_reply(channel, them, msg)
        say(the_reply)
        cast(f"ANNA:{the_reply}")

    # Default: set channel topic in 60 seconds. Can be interrupted by another rejoinder.
    ToT[channel]['rejoinder'] = th.Timer(60, status_report, [say, context])
    ToT[channel]['rejoinder'].start()

    interval = None
    # Long response
    if random.random() < 0.1:
        interval = [9,12]
    # Medium response
    elif random.random() < 0.2:
        interval = [6,8]
    # Quick response
    elif random.random() < 0.3:
        interval = [4,5]

    if interval:
        say_something_later(
            say,
            channel,
            context,
            when=random.randint(interval[0], interval[1])
        )

@app.event("app_mention")
def handle_app_mention_events(body, client, say): # pylint: disable=unused-argument
    ''' Reply to @mentions '''
    channel = body['event']['channel']
    them = get_display_name(body['event']['user'])
    msg = substitute_names(body['event']['text'])

    if channel not in ToT:
        new_channel(channel)

    say(get_reply(channel, them, msg))

if __name__ == "__main__":
    handler = SocketModeHandler(app, os.environ["SLACK_APP_TOKEN"])
    try:
        handler.start()
    # Exit gracefully on ^C (so the wrapper script while loop continues)
    except KeyboardInterrupt:
        raise SystemExit(0)
