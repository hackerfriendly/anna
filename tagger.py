#!/usr/bin/env python
import nltk
import fileinput

sentences = nltk.sent_tokenize('\n'.join(fileinput.input()))
# print(sentences)

words = [nltk.word_tokenize(sent) for sent in sentences]
# print(words)

tags = [nltk.pos_tag(word) for word in words]
# print(tags)

grammar = "NP: {<DT>?<JJ>*<NN>}"
cp = nltk.RegexpParser(grammar)
for tag in tags:
    print(cp.parse(tag), '\n')
