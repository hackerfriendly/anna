#!/usr/bin/env python
import os
import pickle
import pygame
import numpy as np
import PIL.Image
import dnnlib
import dnnlib.tflib as tflib
import config
import random

from IPython.display import display
from subprocess import run
from time import sleep

# pygame
os.putenv('SDL_VIDEODRIVER', 'fbcon')
try:
    pygame.display.init()
except pygame.error:
    print('>>> Are you root?\n')
    raise

size = (pygame.display.Info().current_w, pygame.display.Info().current_h)
screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
screen.fill((0, 0, 0))
pygame.mouse.set_visible(False)
pygame.font.init()
pygame.display.update()

# dataset
subject = 'ffhq'

tflib.init_tf()

datasets = {
    # karras2019stylegan-ffhq-1024x1024.pkl
    'ffhq'     : { 'url': 'https://drive.google.com/uc?id=1MEGjdvVpUsu1jB4zrXZN7Y4kBBOzizDQ', 'w': 1024, 'h': 1024 },
    # karras2019stylegan-celebahq-1024x1024.pkl
    'celebahq' : { 'url': 'https://drive.google.com/uc?id=1MGqJl28pN4t7SAtSrPdSRJSQJqahkzUf', 'w': 1024, 'h': 1024 },
    # karras2019stylegan-bedrooms-256x256.pkl
    'bedrooms' : { 'url': 'https://drive.google.com/uc?id=1MOSKeGF0FJcivpBI7s63V9YHloUTORiF', 'w': 256, 'h': 256 },
    # karras2019stylegan-cars-512x384.pkl
    'cars'     : { 'url': 'https://drive.google.com/uc?id=1MJ6iCfNtMIRicihwRorsM3b7mmtmK9c3', 'w': 512, 'h': 384 },
    # karras2019stylegan-cats-256x256.pkl
    'cats'     : { 'url': 'https://drive.google.com/uc?id=1MQywl0FNt6lHu8E_EUqnRbviagS7fbiJ', 'w':256, 'h':256 },
}

url = datasets[subject]['url']

with dnnlib.util.open_url(url, cache_dir='/home/rob/stylegan/cache') as f:
    _G, _D, Gs = pickle.load(f)

# Gs.print_layers()

# write to framebuffer
# http://seenaburns.com/2018/04/04/writing-to-the-framebuffer/
def fbdisplay(img):
    with open('/dev/fb0', 'wb') as f:
        # for p in list(img.resize((480,480)).getdata()):
        for i, p in enumerate(list(img.getdata())):
            f.write(p[2].to_bytes(1, byteorder='little'))
            f.write(p[1].to_bytes(1, byteorder='little'))
            f.write(p[0].to_bytes(1, byteorder='little'))
            f.write((0).to_bytes(1, byteorder='little'))
            if not i % 1024:
                f.seek(1280 - 1025, 1)

def display(img):
    global screen
    screen.blit(pygame.image.fromstring(img.tobytes(), img.size, img.mode), (128, 0))
    pygame.display.update()

w = datasets[subject]['w']
h = datasets[subject]['h']

synthesis_kwargs = dict(output_transform=dict(func=tflib.convert_images_to_uint8, nchw_to_nhwc=True), minibatch_size=8)

canvas = PIL.Image.new('RGB', (w, h), 'white')

def new_seed():
    global seed, latents, dlatents, dlatent_avg
    seed = np.random.randint(2**32 - 1)
    latents = np.stack([np.random.RandomState(seed).randn(Gs.input_shape[1])])
    dlatents = Gs.components.mapping.run(latents, None) # [seed, layer, component]
    dlatent_avg = Gs.get_var('dlatent_avg') # [component]

def make_faces(psis):
    global seed, latents, dlatents, dlatent_avg    
    for dlatent in list(dlatents):
        for psi in psis:
            row_dlatents = (dlatent[np.newaxis] - dlatent_avg) * np.reshape([psi], [-1, 1, 1]) + dlatent_avg
            row_images = Gs.components.synthesis.run(row_dlatents, randomize_noise=True, **synthesis_kwargs)
            for image in list(row_images):
                display(PIL.Image.fromarray(image, 'RGB'))

def fine_range(a, b, c):
    return [x / 100.0 for x in range(a, b, c)]

# monster mode!
# fast = 200
# dwell = 10
# delta = 180

# normal mode
fast = 7
dwell = 15
normal = 70

# normal -> monster
# monster = 180
monster = 120

running = True
while True:
    if os.path.exists('../.faces-enabled'):
        if not running:
            running = True
            print()

        new_seed()
        delta = np.random.randint(normal, monster)
        print("seed:", seed, "delta:", delta)
        
        # get there fast
        make_faces(fine_range(0, -delta, -fast))
        # dwell
        for i in range(dwell):
            make_faces(fine_range(-delta, -delta + 3, 1))
            make_faces(fine_range(-delta + 3, -delta, -1))
        # get back fast
        make_faces(fine_range(-delta, 0, fast))

        # get there fast
        make_faces(fine_range(0, delta, fast))
        # dwell
        for i in range(dwell):
            make_faces(fine_range(delta, delta - 3, -1))
            make_faces(fine_range(delta - 3, delta, 1))
        # get back fast
        make_faces(fine_range(delta, 0, -fast))
    else:
        if running:
            running = False
        print(random.choice(['z','Z']), end='', flush=True)
        sleep(1)
